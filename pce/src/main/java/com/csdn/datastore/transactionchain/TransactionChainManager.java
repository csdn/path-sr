/**
 * Copyright (c) 2015 Cisco Systems, Inc. and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package com.csdn.datastore.transactionchain;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.FluentFuture;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.google.errorprone.annotations.concurrent.GuardedBy;
import org.eclipse.jdt.annotation.NonNull;
import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.ReadWriteTransaction;
import org.opendaylight.mdsal.binding.api.Transaction;
import org.opendaylight.mdsal.binding.api.TransactionChain;
import org.opendaylight.mdsal.binding.api.TransactionChainClosedException;
import org.opendaylight.mdsal.binding.api.TransactionChainListener;
import org.opendaylight.mdsal.common.api.CommitInfo;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.mdsal.common.api.TransactionCommitFailedException;
import org.opendaylight.yangtools.yang.binding.DataObject;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransactionChainManager implements TransactionChainListener, AutoCloseable {

    private static final Logger LOG = LoggerFactory.getLogger(TransactionChainManager.class);
    private static final String CANNOT_WRITE_INTO_TRANSACTION = "Cannot write into transaction.";

    private final Object txLock = new Object();
    private final DataBroker dataBroker;

    @GuardedBy("txLock")
    private ReadWriteTransaction writeTx;
    @GuardedBy("txLock")
    private TransactionChain transactionChain;
    @GuardedBy("txLock")
    private boolean submitIsEnabled;
    @GuardedBy("txLock")
    private ListenableFuture<Void> lastSubmittedFuture;

    private volatile boolean initCommit;

    @GuardedBy("txLock")
    private TransactionChainManagerStatus transactionChainManagerStatus = TransactionChainManagerStatus.SLEEPING;

    public TransactionChainManager(final DataBroker dataBroker) {
        this.dataBroker = dataBroker;
        this.lastSubmittedFuture = Futures.immediateFuture(null);
    }

    @GuardedBy("txLock")
    private void createTxChain() {
        TransactionChain txChainFactoryTemp = transactionChain;
        transactionChain = dataBroker.createTransactionChain(TransactionChainManager.this);
        Optional.ofNullable(txChainFactoryTemp).ifPresent(TransactionChain::close);
    }

    public boolean initialSubmitWriteTransaction() {
        enableSubmit();
        return submitTransaction();
    }

    /**
     * Method change status for TxChainManager to WORKING and it has to make
     * registration for this class instance as {@link TransactionChainListener} to provide possibility a make DS
     * transactions. Call this method for MASTER role only.
     */
    public void activateTransactionManager() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("activateTransactionManager for transaction submit is set to {}", submitIsEnabled);
        }
        synchronized (txLock) {
            if (TransactionChainManagerStatus.SLEEPING == transactionChainManagerStatus) {
                Preconditions.checkState(transactionChain == null,
                        "TxChainFactory survive last close.");
                Preconditions.checkState(writeTx == null,
                        "We have some unexpected WriteTransaction.");
                this.transactionChainManagerStatus = TransactionChainManagerStatus.WORKING;
                this.submitIsEnabled = false;
                this.initCommit = true;
                createTxChain();
            }
        }
    }
    
    public void deactivateTransactionManager() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("deactivateTransactionManager ");
        }
        synchronized (txLock) {
            if (TransactionChainManagerStatus.WORKING == transactionChainManagerStatus) {
                transactionChainManagerStatus = TransactionChainManagerStatus.SLEEPING;
                txChainShuttingDown();
            }
        }
    }

    private void closeTransactionChain() {
        if (writeTx != null) {
            writeTx.cancel();
            writeTx = null;
        }
        Optional.ofNullable(transactionChain).ifPresent(TransactionChain::close);
        transactionChain = null;
    }

    @GuardedBy("txLock")
    public boolean submitTransaction() {
        return submitTransaction(false);
    }

    @GuardedBy("txLock")
    public boolean submitTransaction(boolean doSync) {
        synchronized (txLock) {
            if (!submitIsEnabled) {
                if (LOG.isTraceEnabled()) {
                    LOG.trace("transaction not committed - submit block issued");
                }
                return false;
            }
            if (Objects.isNull(writeTx)) {
                if (LOG.isTraceEnabled()) {
                    LOG.trace("nothing to commit - submit returns true");
                }
                return true;
            }

            final FluentFuture submitFuture = writeTx.commit();
            lastSubmittedFuture = submitFuture;
            writeTx = null;

            if (initCommit || doSync) {
                try {
                    submitFuture.get(5L, TimeUnit.SECONDS);
                } catch (InterruptedException | ExecutionException | TimeoutException ex) {
                    LOG.error("Exception during INITIAL({}) || doSync({}) transaction submitting. ",
                            initCommit, doSync, ex);
                    return false;
                }
                initCommit = false;
                return true;
            }

            Futures.addCallback(submitFuture, new FutureCallback<CommitInfo>() {
                @Override
                public void onSuccess(final CommitInfo result) {
                    //NOOP
                }

                @Override
                public void onFailure(final Throwable throwable) {
                    if (throwable instanceof TransactionCommitFailedException) {
                        LOG.error("Transaction commit failed. ", throwable);
                    } else {
                        if (throwable instanceof CancellationException) {
                            LOG.warn("Submit task was canceled");
                            LOG.trace("Submit exception: ", throwable);
                        } else {
                            LOG.error("Exception during transaction submitting. ", throwable);
                        }
                    }
                }
            }, MoreExecutors.directExecutor());
        }
        return true;
    }

    public <T extends DataObject> void addDeleteOperationToTxChain(final LogicalDatastoreType store,
                                                                    final InstanceIdentifier<T> path) {
        synchronized (txLock) {
            ensureTransaction();
            if (writeTx == null) {
                LOG.debug("WriteTx is null , Delete {} was not realized.", path);
                throw new TransactionChainClosedException(CANNOT_WRITE_INTO_TRANSACTION);
            }

            writeTx.delete(store, path);
        }
    }

    public <T extends DataObject> void writeToTransaction(final LogicalDatastoreType store,
                                                          final InstanceIdentifier<T> path,
                                                          final T data) {
        synchronized (txLock) {
            ensureTransaction();
            if (writeTx == null) {
                LOG.debug("WriteTx is null . Write data for {} was not realized.", path);
                throw new TransactionChainClosedException(CANNOT_WRITE_INTO_TRANSACTION);
            }

            writeTx.mergeParentStructurePut(store, path, data);
        }
    }

    public <T extends DataObject> void mergeToTransaction(final LogicalDatastoreType store,
                                                          final InstanceIdentifier<T> path,
                                                          final T data,
                                                          final boolean createParents) {
        synchronized (txLock) {
            ensureTransaction();
            if (writeTx == null) {
                LOG.debug("WriteTx is null . Merge data for {} was not realized.", path);
                throw new TransactionChainClosedException(CANNOT_WRITE_INTO_TRANSACTION);
            }

            writeTx.merge(store, path, data, createParents);
        }
    }

    public <T extends DataObject> FluentFuture<Optional<T>>
        readFromTransaction(final LogicalDatastoreType store, final InstanceIdentifier<T> path) {
        synchronized (txLock) {
            ensureTransaction();
            if (writeTx == null) {
                LOG.debug("WriteTx is null. Read data for {} was not realized.", path);
                throw new TransactionChainClosedException(CANNOT_WRITE_INTO_TRANSACTION);
            }

            return writeTx.read(store, path);
        }
    }

    @GuardedBy("txLock")
    private void ensureTransaction() {
        if (writeTx == null && TransactionChainManagerStatus.WORKING == transactionChainManagerStatus
                && transactionChain != null) {
            writeTx = transactionChain.newReadWriteTransaction();
        }
    }

    private void enableSubmit() {
        synchronized (txLock) {
            /* !!!IMPORTANT: never set true without transactionChain */
            submitIsEnabled = transactionChain != null;
        }
    }

    public void shuttingDown() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("TxManager is going SHUTTING_DOWN ");
        }
        synchronized (txLock) {
            this.transactionChainManagerStatus = TransactionChainManagerStatus.SHUTTING_DOWN;
            txChainShuttingDown();
        }
    }

    @GuardedBy("txLock")
    private void txChainShuttingDown() {
        boolean wasSubmitEnabled = submitIsEnabled;
        submitIsEnabled = false;

        if (!wasSubmitEnabled || transactionChain == null) {

            if (writeTx != null) {
                writeTx.cancel();
                writeTx = null;
            }
        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Submitting all transactions");
            }
            // hijack md-sal thread
            writeTx.commit();
            writeTx = null;
        }
    }

    @Override
    public void close() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Setting transactionChainManagerStatus to SHUTTING_DOWN ");
        }
        synchronized (txLock) {
            closeTransactionChain();
        }
    }

    @Override
    public void onTransactionChainFailed(@NonNull TransactionChain chain, @NonNull Transaction transaction, @NonNull Throwable cause) {
        synchronized (txLock) {
            if (TransactionChainManagerStatus.WORKING == transactionChainManagerStatus
                    && chain.equals(this.transactionChain)) {
                LOG.warn("Transaction chain failed, recreating chain due to ", cause);
                closeTransactionChain();
                createTxChain();
                writeTx = null;
            }
        }
    }

    @Override
    public void onTransactionChainSuccessful(@NonNull TransactionChain chain) {
        // NOOP
    }

    private enum TransactionChainManagerStatus {
        /**
         * txChainManager is working - is active (MASTER).
         */
        WORKING,
        /**
         * txChainManager is sleeping - is not active (SLAVE or default init value).
         */
        SLEEPING,
        /**
         * txChainManager is trying to be closed - device disconnecting.
         */
        SHUTTING_DOWN
    }
}

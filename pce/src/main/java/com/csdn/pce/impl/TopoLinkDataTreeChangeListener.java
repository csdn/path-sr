package com.csdn.pce.impl;

import com.csdn.pce.api.Link;
import com.csdn.pce.api.Node;
import org.eclipse.jdt.annotation.NonNull;
import org.opendaylight.mdsal.binding.api.ClusteredDataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.yang.gen.v1.com.csdn.topology.rev191227.topology.StoreLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class TopoLinkDataTreeChangeListener implements ClusteredDataTreeChangeListener<StoreLink> {

    private static final Logger LOG = LoggerFactory.getLogger(TopoLinkDataTreeChangeListener.class);
    private final PceServiceImpl pceService;
    public TopoLinkDataTreeChangeListener(PceServiceImpl pceService) {
        this.pceService = pceService;
    }

    @Override
    public void onDataTreeChanged(@NonNull Collection<DataTreeModification<StoreLink>> changes) {
        for (DataTreeModification<StoreLink> change : changes) {
            final DataObjectModification<StoreLink> mod = change.getRootNode();
            switch (mod.getModificationType()) {
                case DELETE:
                    break;
                case SUBTREE_MODIFIED:
                case WRITE:
                    StoreLink storeLink = mod.getDataAfter();
                    Link link = new Link(storeLink.getSourceId(),storeLink.getSourcePort()
                            ,storeLink.getDestinationId(),storeLink.getDestinationPort());
                    Node source = new Node(storeLink.getSourceId());
                    Node destination = new Node(storeLink.getSourceId());
                    pceService.addLinkSync(link,source,destination);
                    break;
                default:
                    LOG.error("Unhandled modification type " + mod.getModificationType());
                    break;
            }
        }
    }
}

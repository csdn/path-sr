/*
 * Copyright © 2018 csdn and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.csdn.pce.impl;

import static org.opendaylight.mdsal.common.api.LogicalDatastoreType.OPERATIONAL;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.yang.gen.v1.com.csdn.topology.rev191227.Topology;
import org.opendaylight.yang.gen.v1.com.csdn.topology.rev191227.topology.StoreLink;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PceInstance {

    private static final Logger LOG = LoggerFactory.getLogger(PceInstance.class);

    ServerBootstrap serverBootstrap = new ServerBootstrap();
    EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    EventLoopGroup workerGroup = new NioEventLoopGroup();
    ChannelFuture channelFuture;
    Channel channel;
    private PceServiceImpl pceService;
    private final DataBroker dataBroker;
    private final TopoLinkDataTreeChangeListener linkDataTreeChangeListener;
    static InstanceIdentifier<StoreLink> LINKID = InstanceIdentifier.create(Topology.class)
            .child(StoreLink.class);
    static DataTreeIdentifier<StoreLink> IID = DataTreeIdentifier.create(OPERATIONAL, LINKID);

    public PceInstance(PceServiceImpl networktopo, final DataBroker dataBroker) {
        this.pceService = networktopo;
        this.dataBroker = dataBroker;
        linkDataTreeChangeListener = new TopoLinkDataTreeChangeListener(pceService);
        this.dataBroker.registerDataTreeChangeListener(IID, linkDataTreeChangeListener);
    }

    /**
     * Method called when the blueprint container is created.
     */
    public void init() {
        LOG.info("PceInstance Session Initiated");

        serverBootstrap.group(bossGroup,workerGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG,1024)
                .childOption(ChannelOption.TCP_NODELAY,true)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ServerInitializer(dataBroker,pceService));
        channelFuture = serverBootstrap.bind(6666);

        try {
            this.channel = channelFuture.sync().channel();
        } catch (InterruptedException e) {
            LOG.warn("channel close ex:",e);
        }
    }

    /**
     * Method called when the blueprint container is destroyed.
     */
    public void close() {
        LOG.info("PceInstance Closed");
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
    }
}
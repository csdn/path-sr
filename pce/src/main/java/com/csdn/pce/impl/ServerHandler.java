package com.csdn.pce.impl;

import static org.opendaylight.mdsal.common.api.LogicalDatastoreType.OPERATIONAL;

import com.csdn.datastore.transactionchain.OperationProcessor;
import com.csdn.pce.api.Node;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.net.InetAddress;
import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.yang.gen.v1.com.csdn.topology.rev191227.Topology;
import org.opendaylight.yang.gen.v1.com.csdn.topology.rev191227.topology.*;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Sharable
public class ServerHandler extends SimpleChannelInboundHandler<String> {

    private static final Logger LOG = LoggerFactory.getLogger(ServerHandler.class);
    private DataBroker dataBroker;
    private static int conn_count = 1;
    private OperationProcessor processor;

    private PceServiceImpl pceService;

    public ServerHandler() {

    }

    public void setPceService(PceServiceImpl pceService) {
        this.pceService = pceService;
    }

    public void setDataBroker(final DataBroker dataBroker) {
        this.dataBroker = dataBroker;
        processor = new OperationProcessor(dataBroker);
        processor.start();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // 为新连接发送庆祝
        ctx.write("Welcome to " + InetAddress.getLocalHost().getHostName() + "!\r\n");
        ctx.write("You are the No. " + (conn_count ++) + " user.\r\n");
        ctx.flush();
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, String input) {

        //LOG.info(input);
        String[] tmp = input.split(";");
        if (tmp[0].equalsIgnoreCase("link")) {

            processor.enqueueOperation( chain->{
                    InstanceIdentifier<StoreLink> linkid = InstanceIdentifier.create(Topology.class)
                            .child(StoreLink.class, new StoreLinkKey(Long.parseLong(tmp[1]),Integer.parseInt(tmp[2]),
                                    Long.parseLong(tmp[3]),Integer.parseInt(tmp[4])));

                    StoreLink storelink = new StoreLinkBuilder()
                            .setSourceId(Long.parseLong(tmp[1]))
                            .setSourcePort(Integer.parseInt(tmp[2]))
                            .setDestinationId(Long.parseLong(tmp[3]))
                            .setDestinationPort(Integer.parseInt(tmp[4]))
                            .withKey(new StoreLinkKey(Long.parseLong(tmp[1]),Integer.parseInt(tmp[2]),
                                    Long.parseLong(tmp[3]),Integer.parseInt(tmp[4])))
                            .build();

                    chain.writeToTransaction(OPERATIONAL, linkid, storelink);
                });

        } else if (tmp[0].equalsIgnoreCase("source")) {
            ctx.write(pceService.getIncomingEdgeMap(new Node(Integer.parseInt(tmp[1]))).toString() + ".\r\n");
            ctx.flush();
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        LOG.warn(cause.getMessage());
        ctx.close();
    }
}

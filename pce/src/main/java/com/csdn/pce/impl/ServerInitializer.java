package com.csdn.pce.impl;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.opendaylight.mdsal.binding.api.DataBroker;

public class ServerInitializer extends ChannelInitializer<SocketChannel> {
    private static final StringEncoder ENCODER = new StringEncoder();
    private static final StringDecoder DECODER = new StringDecoder();
    private static final ServerHandler SERVER_HANDLER = new ServerHandler();

    public ServerInitializer(final DataBroker dataBroker, PceServiceImpl pceService) {
        SERVER_HANDLER.setDataBroker(dataBroker);
        SERVER_HANDLER.setPceService(pceService);
    }

    @Override
    public void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        // Add the text line codec combination first,
        //pipeline.addLast(new FixedLengthFrameDecoder(256));
        // the encoder and decoder are static as these are sharable
        pipeline.addLast(DECODER);
        pipeline.addLast(ENCODER);

        // and then business logic. 插入Disruptor队列
        pipeline.addLast(SERVER_HANDLER);

    }
}
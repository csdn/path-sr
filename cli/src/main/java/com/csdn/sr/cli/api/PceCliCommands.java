/*
 * Copyright © 2018 CSDN and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.csdn.sr.cli.api;

public interface PceCliCommands {

    /**
     * Define the Karaf command method signatures and the Javadoc for each.
     * Below method is just an example
     */
    String queryCommand(String query);

    String shortestPathCommand(int source, int destination);
}

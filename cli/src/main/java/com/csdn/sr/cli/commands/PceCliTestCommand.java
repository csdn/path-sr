/*
 * Copyright © 2018 CSDN and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.csdn.sr.cli.commands;

import com.csdn.sr.cli.api.PceCliCommands;
import org.apache.karaf.shell.commands.Command;
import org.apache.karaf.shell.commands.Option;
import org.apache.karaf.shell.console.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class name can be renamed to match the command implementation that it will invoke.
 * Specify command details by updating the fields in the Command annotation below.
 */
@Command(name = "pce", scope = "pce",
        description = "display pce info")
public class PceCliTestCommand extends AbstractAction {

    private static final Logger LOG = LoggerFactory.getLogger(PceCliTestCommand.class);
    protected final PceCliCommands service;

    public PceCliTestCommand(final PceCliCommands service) {
        this.service = service;
    }

    /**
     * Add the arguments required by the command.
     * Any number of arguments can be added using the Option annotation
     */
    @Option(name = "-q",
            aliases = { "--query" },
            description = "query count of node and link",
            required = true,
            multiValued = false)
    private String query;

    @Option(name = "-s",
            aliases = { "--source" },
            description = "source of path",
            required = false,
            multiValued = false)
    private String source;

    @Option(name = "-d",
            aliases = { "--destination" },
            description = "destination of path",
            required = false,
            multiValued = false)
    private String destination;

    @Override
    protected Object doExecute() throws Exception {

        String retMessage = "";
        if (query.equalsIgnoreCase("count")) {
            retMessage = service.queryCommand(query);
        }
        if (source != null && !source.isEmpty() && destination != null && !destination.isEmpty()) {
            retMessage = retMessage + "\n"
                    + service.shortestPathCommand(Integer.parseInt(source), Integer.parseInt(destination));
        }
        return retMessage;
    }
}

/*
 * Copyright © 2018 CSDN and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.csdn.pce.api;

public class Node {

    public Node(long id) {
        nodeId = id;
    }

    public long getNodeId() {
        return nodeId;
    }

    public void setNodeId(long id) {
        this.nodeId = id;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof Node)) {
            return false;
        }
        if (this == other) {
            return true;
        }

        return nodeId == ((Node)other).getNodeId();
    }

    @Override
    public int hashCode() {
        return (int)this.nodeId;
    }

    private long nodeId;
}

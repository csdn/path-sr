/*
 * Copyright © 2018 CSDN and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.csdn.pce.api;

import java.util.List;

public interface PceService<V,E> {

    int getLinkCount();

    int getNodeCount();

    List<E> getPath(V source, V target);
}

/*
 * Copyright © 2018 CSDN and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.csdn.pce.api;

public class Link {

    public Link(long localId, int localPort, long remoteId, int remotePort) {
        this.localSystemId = localId;
        this.localPort = localPort;
        this.remoteSystemId = remoteId;
        this.remotePort = remotePort;
    }

    public long getLocalSystemId() {
        return localSystemId;
    }

    public void setLocalSystemId(long localSystemId) {
        this.localSystemId = localSystemId;
    }

    public long getRemoteSystemId() {
        return remoteSystemId;
    }

    public void setRemoteSystemId(long remoteSystemId) {
        this.remoteSystemId = remoteSystemId;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public int getLocalPort() {
        return localPort;
    }

    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }

    @Override
    public String toString() {
        return remoteSystemId + ":" + remotePort + "->" + localSystemId + ":" + localPort;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof Link)) {
            return false;
        }
        if (this == other) {
            return true;
        }
        Link otherLink = (Link)other;
        return this.localSystemId == otherLink.getLocalSystemId()
                && this.localPort == otherLink.getLocalPort()
                && this.remoteSystemId == otherLink.getRemoteSystemId()
                && this.remotePort == otherLink.getRemotePort();
    }

    @Override
    public int hashCode() {
        return (int)localSystemId + localPort + (int)remoteSystemId + remotePort;
    }

    private long localSystemId;
    private int localPort;
    private long remoteSystemId;
    private int remotePort;
}
